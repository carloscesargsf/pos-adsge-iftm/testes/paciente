package com.example.demo;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class FilaTest {

    private Fila fila;

    @BeforeEach
    void setUp() {
        fila = new Fila();
    }

    @Test
    void testaPrioridade() {
        Paciente p1 = new Paciente("Carlos", 31, Prioridade.VERDE);
        fila.addPaciente(p1);

        Paciente p2 = new Paciente("Fabíola", 31, Prioridade.VERMELHA);
        fila.addPaciente(p2);

        Paciente p3 = new Paciente("César", 57, Prioridade.AMARELA);
        fila.addPaciente(p3);

        Paciente p4 = new Paciente("Vania", 55, Prioridade.VERMELHA);
        fila.addPaciente(p4);

        Paciente p5 = new Paciente("Danilo", 29, Prioridade.AMARELA);
        fila.addPaciente(p5);

        Paciente p6 = new Paciente("Tathi", 27, Prioridade.VERDE);
        fila.addPaciente(p6);

        Paciente r1 = fila.getPaciente();
        Paciente r2 = fila.getPaciente();
        Paciente r3 = fila.getPaciente();
        Paciente r4 = fila.getPaciente();
        Paciente r5 = fila.getPaciente();
        Paciente r6 = fila.getPaciente();

        assertEquals(Prioridade.VERMELHA, r1.getPrioridade());
        assertEquals(Prioridade.VERMELHA, r2.getPrioridade());
        assertEquals(Prioridade.AMARELA, r3.getPrioridade());
        assertEquals(Prioridade.AMARELA, r4.getPrioridade());
        assertEquals(Prioridade.VERDE, r5.getPrioridade());
        assertEquals(Prioridade.VERDE, r6.getPrioridade());
    }

}