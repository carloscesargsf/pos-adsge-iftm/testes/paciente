package com.example.demo;

import java.util.Objects;

public class Paciente implements Comparable<Paciente> {

    private String nome;

    private Integer idade;

    private Prioridade prioridade;

    public Paciente(String nome, Integer idade, Prioridade prioridade) {
        setNome(nome);
        setIdade(idade);
        setPrioridade(prioridade);
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Integer getIdade() {
        return idade;
    }

    public void setIdade(Integer idade) {
        this.idade = idade;
    }

    public Prioridade getPrioridade() {
        return prioridade;
    }

    public void setPrioridade(Prioridade prioridade) {
        this.prioridade = prioridade;
    }

    @Override
    public int compareTo(Paciente paciente) {
        return getPrioridade().getOrdem().equals(paciente.getPrioridade().getOrdem()) ? 0 : (
                getPrioridade().getOrdem() > paciente.getPrioridade().getOrdem() ? 1 : -1);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Paciente paciente = (Paciente) o;
        return nome.equals(paciente.nome) &&
                idade.equals(paciente.idade);
    }

    @Override
    public int hashCode() {
        return Objects.hash(nome, idade);
    }

}
