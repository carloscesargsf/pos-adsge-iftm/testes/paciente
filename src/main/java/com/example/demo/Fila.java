package com.example.demo;

import java.util.PriorityQueue;

public class Fila {

    private PriorityQueue<Paciente> fila = new PriorityQueue<>();

    public Fila() {
        fila = new PriorityQueue<>();
    }

    public void addPaciente(Paciente paciente) {
        fila.add(paciente);
    }

    public Paciente getPaciente() {
        return fila.poll();
    }

    public Integer getTamanhoFila() {
        return fila.size();
    }

}
