package com.example.demo;

public enum Prioridade {

    VERMELHA(1),
    AMARELA(2),
    VERDE(3);

    private Integer ordem;

    Prioridade(Integer ordem) {
        setOrdem(ordem);
    }

    public Integer getOrdem() {
        return ordem;
    }

    private void setOrdem(Integer ordem) {
        this.ordem = ordem;
    }

}
